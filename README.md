AWMC Leaflet
------------
   
 * Introduction

This module adds the map tiles provided by the Ancient World Mapping Center (AWMC) hosted at MapBox (http://mapbox.com). For more information about these layers see http://awmc.unc.edu/wordpress/tiles/map-tile-information

 * Requirements

Leaflet (https://www.drupal.org/project/leaflet)

 * Installation

Download and enable this module. The AWMC layers will appear when selecting a style for the Leaflet map.

 * Maintainers

Andrew Larcombe (alarcombe) https://www.drupal.org/user/172759
